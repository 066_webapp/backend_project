import {
  IsEmail,
  IsInt,
  IsNotEmpty,
  IsPositive,
  Length,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateEmployeeDto {
  @Length(4, 16)
  @IsNotEmpty()
  username: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;

  @MinLength(4)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  age: number;

  @Matches(/^[0-9]*$/)
  @MaxLength(10)
  @IsNotEmpty()
  tel: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @MinLength(4)
  @IsNotEmpty()
  role: string;
}
